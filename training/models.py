# -*- coding: utf-8 -*-

from openerp import models, fields, api,http


class training(models.Model):
	_name = 'training.training'


        Trainer_name = fields.Char()
        session_name = fields.Char()
        session_time = fields.Integer()
        student_name = fields.Char()
        salary = fields.Char()
        datt = fields.Date()
        end_datt = fields.Date()
        hours = fields.Float()


class chessclub(models.Model):
	_name = 'training.chess'
        name = fields.Char('Name', required=True, translate=True)
        #image = fields.Image("Image",
                              #help="This field holds the image used as image for our customers, limited to 1024x1024px.",null=True)
        trainer = fields.Char()
        student = fields.Char()
        trainer_title = fields.Char()
        trainer_ratting = fields.Integer()
        student_title = fields.Char(null=True)
        student_ratting = fields.Integer()
        joining_date = fields.Date()
        expire_date = fields.Date()
        session_time_start = fields.Integer()
        session_time_end = fields.Integer()
        hours = fields.Float()


class academy(http.Controller):
    _name = 'training.academy'

    @http.route('/training/training/', auth='public')
    def index(self, **kw):
        return http.request.render('training.index', {
            'players': ["amit", "ajay", "irfan","dinesh","rajen"],
        })

    @http.route('/training/chessclub/', auth='public')
    def indexx(self, **kw):
        chessclub = http.request.env['training.chess']
        return http.request.render('training.chesstrainer', {'teachers': chessclub.search([])})


class Academy(http.Controller):
    @http.route('/academy/academy/', auth='public')
    def index(self, **kw):
        Teachers = http.request.env['academy.teachers']
        return http.request.render('academy.index', {
            'teachers': Teachers.search([])
        })

